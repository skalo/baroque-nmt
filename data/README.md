# DATA FOLDER


## Subfolders

### `baroque_large_trainset_twopart`
Several folders (training & validation sets with recombined MIDI files meaning that they have two parts extracted from tracks with possibly more than two tracks).

### `baroque_testset`
A hundred MIDI files of varying quality.

### `baroque_testset_twopart`
28 high-quality MIDI files.
