# Dataset creation

Update `config.py`, which has folder paths such as `DATA_FOLDER`. Make sure these are all up-to-date.

This repo includes the training and testing datasets that were used to run the experiments in the paper. 
To reproduce the results using this data, skip to the section "Dataset processing" below.

Optional: To generate a new dataset from your own collection of midi files, follow these steps (N.B. details vary based
on the format of your MIDI tracks, so custom python code may be necessary):

1. Go into `src/preprocess` and run:

   a. `pick_folders.py` (if you need to select a certain subset of whitelisted composers/folders from your 
   MIDI collection). This just prints a list of MIDI files to be used by later steps to verify what will happen.
   
   b. `extract_files.py`
   
   c. `extract_files_subset.py` (optional; this removes duplicate file names)
   
   d. `extract_parts.py`  (does the work of separating into midi files that each contain just 2 parts)
   
2. Put MIDI files in `<DATA_FOLDER>/midi`  (remember to add the "midi" subfolder and put things there!)

# Dataset processing
1. cd to the `src` folder.
2. Convert to 4-measure chunks, separate into train/validate, and transpose up and down by running:

     `./make_datasets.sh`

     N.B. This can take a long time, but takes advantage of multiple CPU cores if available. 
     I run it on a 16-core machine to avoid waiting all day.

Verify you have 2 files (all_input.txt, all_output.txt) in each dir: TRAIN_FOLDER_AUG, VALIDATION_FOLDER_AUG

This is the dataset to use for model training.


Note: you may need to modify the DATA_FOLDER in config.py and repeat steps 1-3 to create the TUPART dataset for testing.
    You may also need to modify train/test split for TUPART here to pare down the parts for human validation.

# Model training

1. `cd src/transformer`
2. Update options/paths in `train_and_generate.sh`
3. Run `. train_and_generate.py`
4. Let it run to max epochs, or just stop it at some point. Make a note of the epoch at which you'd like to generate results.
5. Update options in shuffle_data.py and run it to shuffle the input test data before generating outputs. E.g. so we don't just get Albinoni in the first 100 examples.
5. Run `train_and_generate.sh gen <MODEL_DIR> <MODEL_STEP>` -- this puts it in generate-only mode.
6. Run `train_and_generate.sh midi <MODEL_DIR> <MODEL_STEP>` -- this generates the final midi files.

