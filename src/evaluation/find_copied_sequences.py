##############################################################

# Usage: find_copied_sequences.py  generated_sequences.txt training_target_sequences.txt training_target_musical_piece_names.txt

##############################################################


import editdistance
import sys
import multiprocessing as mp
from itertools import repeat

#given a training target sequence and a generated sequence),  find their edit distance
def compare_source_target(target_sequence, gen_sequence):
    edit_distance = editdistance.eval(target_sequence, gen_sequence)
    return edit_distance

if __name__ == "__main__":

    #get file names from command line inputs

    if(len(sys.argv)!=4):
        print("Three command line arguments required: generated sequences file path, training set sequences file path, training set piece names file path")

    generated_seq_filename = sys.argv[1]
    target_filename = sys.argv[2]
    allsrcfilenames = sys.argv[3]
    
    # read into memory all of the small file
    generated_seq_lines = open(generated_seq_filename).readlines()
    generated_seq_lines = [line.strip().split(" ") for line in generated_seq_lines]

    # also into memory - the names of all pieces
    allsrcfilenames = open(allsrcfilenames).readlines()


    pool = mp.Pool(mp.cpu_count())

    #store the closest match found so far and its edit distance and the index of the line where it was found
    closest_match = [[100000,'', 0] for i in range(len(generated_seq_lines))]

    progress_counter = 0

    #filelength = 490104
    with open(target_filename) as target_file:
        for target_line in target_file:
            target_line = target_line.strip().split(" ")
            results = pool.starmap(compare_source_target, zip(generated_seq_lines, repeat(target_line)))

            for ind, edit_distance in enumerate(results):
                if(edit_distance<closest_match[ind][0]):

                    closest_match[ind][0]= edit_distance
                    closest_match[ind][1] = target_line
                    closest_match[ind][2] = progress_counter
            progress_counter += 1
            #if(progress_counter%5000==0):
            #    print("{0:10.2f}% done".format(100*(progress_counter/filelength)))

    count_zero_edit_distance = 0
    for ind, line in enumerate(generated_seq_lines):
        print("Sequence number in generated sequences file is {}".format(ind+1))
        print("Generated sequence: {}".format(line))
        print("The closest line from targets has {} edit distance".format(closest_match[ind][0]))
        if(closest_match[ind][0]==0):
            count_zero_edit_distance+=1
        print("Closest found match: {}".format(closest_match[ind][1]))
        print(allsrcfilenames[closest_match[ind][2]])
        print("Index line of the closest match is {}".format(closest_match[ind][2]+1))
        print("\n")
    print("Found {} generated sequences which were directly copied (edit distance=0)".format(count_zero_edit_distance))
