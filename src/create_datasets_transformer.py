import glob
import os

from config import TRAIN_FOLDER, VALIDATION_FOLDER, REMOVE_HOMOPHONIC


def make_parallel_inout_file(data_folder):
    """
    Given a data folder containing the txt encoding of MIDI files, collect all the inputs and outputs in two parallel
    txt files. Each initial txt encoding needs to contain all the information in one single line. Moreover, the path
    of the higher part should be encoded with 0, while the lower with 1, following the output of midi_to_txt.py

    :param data_folder: where all the midi parts are stored
    :return: writes two files in data_folder, all_input.txt and all_output.txt, which store all MIDI parts
    """
    file_result_in_path = os.path.join(data_folder, 'all_input.txt')
    file_result_out_path = os.path.join(data_folder, 'all_output.txt')
    file_filenames_out_path = os.path.join(data_folder, 'all_src_filenames.txt')

    print('Creating file {}'.format(file_result_in_path))
    print('Creating file {}'.format(file_result_out_path))
    print('Creating file {}'.format(file_filenames_out_path))

    with open(file_result_in_path, 'w') as file_result_in:
        with open(file_result_out_path, 'w') as file_result_out:
            with open(file_filenames_out_path, 'w') as file_filenames:
                for file_in in sorted(glob.glob(os.path.join(data_folder, '*.txt'))):
                    filename = os.path.basename(file_in)
                    # Find higher voice. We'll put it in the "input" file.
                    if filename.split("_")[-2] == "0":  # 0 and 1 encode higher voice and lower voice, respectively
                        pass
                    elif filename.split("_")[-2] == "1":  # if it's the lower hand, skip this file
                        continue
                    elif filename == 'all_input.txt' or filename == 'all_output.txt' or filename == 'all_src_filenames.txt':
                        continue
                    else:
                        print("File name {} not in the supported format".format(filename))
                        continue

                    # Find the corresponding lower-hand part.
                    file_out = file_in.split('_')
                    file_out[-2] = '1'
                    file_out = '_'.join(file_out)

                    with open(file_in, "r") as f:
                        linein = f.read().strip()

                    with open(file_out, "r") as f:
                        lineout = f.read().strip()

                    # Skip empty lines. We require a line in both input and output files.
                    if linein and lineout and not (REMOVE_HOMOPHONIC and are_homophonic(linein, lineout)):
                        file_result_in.write(linein + '\n')
                        file_result_out.write(lineout + '\n')
                        file_filenames.write(filename + '\n')


def are_homophonic(line1, line2):
    """
    Check if two lines are homophonic. Every note in the line should be encoded with three ordered tokens:
     - beat position, a string encoding the distance from the initial beat of the chunk
     - pitch, an integer with the MIDI number of the note
     - duration, a string encoding the total duration of the note in terms of the unit time of the piece

    Two lines are defined as homophonic if they have exactly the same notes modulus their octave
    :param line1:
    :param line2:
    :return:
    """
    if not line1 or not line2:
        return False
    x1, x2 = line1.split(" "), line2.split(" ")
    b1, p1, d1 = x1[::3], [int(_) for _ in x1[1::3]], x1[2::3]
    b2, p2, d2 = x2[::3], [int(_) for _ in x2[1::3]], x2[2::3]
    N1, N2 = len(b1), len(b2)
    if N1 != N2:  # if they have a different number of notes they are not homophonic
        return False

    same_beat_position = all([b1[i] == b2[i] for i in range(N1)])
    # notice that if octaves change we still consider it homophonic
    # this check is done note by note so a part of the melody could be at the unison and another at a different octave
    same_pitch_class = all([(p1[i] - p2[i]) % 12 == 0 for i in range(N1)])
    same_duration = all([d1[i] == d2[i] for i in range(N1)])
    if same_beat_position and same_pitch_class and same_duration:
        return True
    return False


if __name__ == '__main__':
    print('Creating dataset')
    make_parallel_inout_file(TRAIN_FOLDER)
    make_parallel_inout_file(VALIDATION_FOLDER)
    print('Done.')
