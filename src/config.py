""" CONFIG """
import os

SIG_DIGITS = 4
CHUNK_SIZE = 4  # MEASURES

# Index (order) of each symbol in the txt file representation.
# Was here for experimentation, but unused for now. 
# Currently serves as documentation for the "language" order of tokens.
SYMBOL_ORDER_POS = 0  # beat position
SYMBOL_ORDER_MID = 1  # midi #
SYMBOL_ORDER_DUR = 2  # duration

#######################################################
# EXPERIMENT SETTINGS

# Set to True to remove "homophonic" examples from training data.
REMOVE_HOMOPHONIC = False

# Set to True to set position token to a dummy value. If False, create string-based position token.
NULL_POSITION_TOKEN = False

# Set to True to take beat position mod Measure Length
BEAT_POSITION_MOD_MEASURE_LENGTH = False

# Set to True to make chunks starting at all measure #s, not skipping by CHUNK_SIZE. (Increases dataset by * CHUNK_SIZE)
MAKE_CHUNKS_FROM_ALL_MEASURE_STARTS = False

CREATE_HUMAN_TESTSET = False  # TODO: still have to run select_human_testset.py by hand


if CREATE_HUMAN_TESTSET:
    TRAIN_TEST_SPLIT = 1  # for test dataset
    DATA_FOLDER = os.path.join('..', 'data', 'baroque_testset_twopart')
else:
    TRAIN_TEST_SPLIT = 0.99  # for large dataset
    DATA_FOLDER = os.path.join('..', 'data', 'baroque_large_trainset_twopart')

#######################################################

# PATHS


NUM_SEGMENTS_HUMAN_EVAL = 100

AUGMENTED_DATA_FOLDER = os.path.join(DATA_FOLDER, 'augmented')
MIDI_FOLDER = os.path.join(DATA_FOLDER, 'midi')
TXT_FOLDER = os.path.join(DATA_FOLDER, 'txt')
TRAIN_FOLDER = os.path.join(DATA_FOLDER, 'training_set')
VALIDATION_FOLDER = os.path.join(DATA_FOLDER, 'validation_set')
TRAIN_FOLDER_AUG = os.path.join(AUGMENTED_DATA_FOLDER, 'training_set')
VALIDATION_FOLDER_AUG = os.path.join(AUGMENTED_DATA_FOLDER, 'validation_set')

# Referenced in code but unused currently
DURATION_TOKEN_DICTIONARY_PATH = os.path.join(TXT_FOLDER, 'dur2tok.pkl')

# FOLDER CREATION
for folder in [DATA_FOLDER, AUGMENTED_DATA_FOLDER, MIDI_FOLDER, TRAIN_FOLDER, VALIDATION_FOLDER,
               TXT_FOLDER, TRAIN_FOLDER_AUG, VALIDATION_FOLDER_AUG]:
    os.makedirs(folder, exist_ok=True)

