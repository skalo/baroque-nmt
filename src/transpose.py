import glob
import os

from config import TRAIN_FOLDER, VALIDATION_FOLDER, TRAIN_FOLDER_AUG, VALIDATION_FOLDER_AUG


def transpose(folder_in, folder_out, input_filenames=None, filenames_file=None, no_transpose=False):
    """
    Reads files from folder_in and writes same name to folder_out, with each version
    augmented with transpositions. If input_filenames is specified, only those filenames are read.

    :param folder_in:
    :param folder_out:
    :param input_filenames:
    :return:
    """

    if input_filenames:
        files = [os.path.join(folder_in, f) for f in input_filenames]
    else:
        files = sorted(glob.glob(os.path.join(folder_in, '*.txt')))

    if filenames_file:
        with open(os.path.join(folder_in, filenames_file), 'r') as filenames_f:
            filenames = [line.rstrip() for line in filenames_f.readlines()]
    else:
        filenames = None

    for f in files:
        print(f'Transposing file {f}')
        with open(f) as in_file:
            in_lines = in_file.readlines()

        basename = os.path.basename(f)
        out_filenames = []
        with open(os.path.join(folder_out, basename), 'w') as out_file:
            for interval in range(-6, 6):  # Transpose up to a tritone down and to a perfect fourth up.
                if no_transpose and (interval != 0):
                    continue

                for k, line in enumerate(in_lines):
                    new_line = []
                    for i, token in enumerate(line.strip().split()):
                        new_token = token
                        # Copy 1st and 3rd tokens of 3 (beat_position and duration)
                        # Transpose 2nd of 3 tokens (midi) unless rest (0).
                        if i % 3 == 1 and token != '0':
                            new_token = str(int(token) + interval)
                        new_line.append(new_token)
                    out_file.write(' '.join(new_line) + '\n')
                    if filenames:
                        out_filenames.append(f'{filenames[k]}_transpose_{interval}')
        if out_filenames:
            with open(os.path.join(folder_out, filenames_file), 'w') as out_filenames_file:
                out_filenames_file.writelines(f'{x}\n' for x in out_filenames)


if __name__ == '__main__':
    names = ('all_input.txt', 'all_output.txt')
    transpose(TRAIN_FOLDER, TRAIN_FOLDER_AUG, names, filenames_file='all_src_filenames.txt')
    transpose(VALIDATION_FOLDER, VALIDATION_FOLDER_AUG, names, filenames_file='all_src_filenames.txt', 
              no_transpose=True)
