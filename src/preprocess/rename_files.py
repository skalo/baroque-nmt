import os

if __name__ == '__main__':
    # DATA_FOLDER = os.path.join('..', '..', 'data', 'barOK', 'Baroque_Contrapuntal')
    DATA_FOLDER = os.path.join('..', '..', 'data', 'clean_dataset', 'two_and_three_parts')
    old = 'bach_js-the_'
    new = 'bach_js-'
    old_files = [f for f in os.listdir(DATA_FOLDER) if f.startswith(old)]
    for src in old_files:
        os.rename(os.path.join(DATA_FOLDER, src), os.path.join(DATA_FOLDER, src.replace(old, new).lower()))
