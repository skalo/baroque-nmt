""" Check if all tracks in the given MIDI are monophonic """

import os

from mido import MidiFile


def _midi_number_to_note(n):
    notes = ['C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'G#', 'A', 'Bb', 'B']
    octave = (n // 12) - 1
    pitch_class = n % 12
    return notes[pitch_class] + str(octave)


def is_monophonic(track, detailed=False):
    """Returns True if MidiTrack is monophonic and has at least min_notes notes."""
    previous_note = 0
    num_notes = 0
    total_time = 0
    monophonic = True
    # Look at each note-on. Find the corresponding note_off.
    for i, msg in enumerate(track):
        total_time += msg.time
        if msg.type == 'note_on' and msg.velocity > 0:
            if previous_note != 0:
                overlap = True
                # Check for a note off at time-delta 0 for the current note before confirming overlap.
                for j in range(i + 1, len(track)):  # Check for events at the same time
                    msg2 = track[j]
                    if msg2.time > 0:  # We moved ahead in time. It really overlaps.
                        break
                    if msg2.type == 'note_off' or (msg2.type == 'note_on' and msg2.velocity == 0):
                        if msg2.note == previous_note:  # the note was actually released just in time; no overlap
                            overlap = False
                            break
                if overlap:
                    monophonic = False
                    if detailed:
                        msg_previous = track[i - 1]
                        ticks_per_beat = midifile.ticks_per_beat
                        beats_per_measure = ts_n * 4 / ts_d
                        measure = int(total_time // (ticks_per_beat * beats_per_measure) + 1)
                        beat = int((total_time % (ticks_per_beat * beats_per_measure)) // ticks_per_beat + 1)
                        print(
                            f'note {_midi_number_to_note(msg.note)} '
                            f'overlaps prev note {_midi_number_to_note(previous_note)} '
                            f'at measure {measure} beat {beat}, '
                            f'time signature = {ts_n} / {ts_d}')
                    else:
                        return monophonic

            previous_note = msg.note
            num_notes += 1
        elif msg.type == 'note_off' or (msg.type == 'note_on' and msg.velocity == 0):
            if msg.note == previous_note:
                previous_note = 0
    return monophonic


if __name__ == '__main__':

    # DETAILED = False
    DETAILED = True

    # DATA_FOLDER = os.path.join('..', '..', 'data', 'barOK', 'Baroque_Concertos_and_Orquestral')
    # DATA_FOLDER = os.path.join('..', '..', 'data', 'barOK', 'Baroque_Contrapuntal')
    # DATA_FOLDER = os.path.join('..', '..', 'data', 'baroque_large_trainset-cleaning')
    DATA_FOLDER = os.path.join('..', '..', 'data', 'clean_dataset', 'two_and_three_parts')
    # DATA_FOLDER = os.path.join('..', '..', 'data', 'clean_dataset', 'more_parts')

    file_paths = sorted(os.listdir(DATA_FOLDER))
    stop = False
    for f in file_paths:
        # if f[:9] != 'GF_Handel':
        #     continue
        # f = 'handel_organ_concerto_4_2_2_(c)icking-archive.mid'
        try:
            midifile = MidiFile(os.path.join(DATA_FOLDER, f))
        except Exception as e:
            print(f'File {f} read failure: {type(e), e}. Skipping.')
            continue
        monophonic = True
        ts_n = 4
        for i, track in enumerate(midifile.tracks):
            if i == 0:
                for msg in track:
                    if msg.type == 'time_signature':
                        ts_n = msg.numerator
                        ts_d = msg.denominator
            if not is_monophonic(track, DETAILED):
                monophonic = False
                if DETAILED:
                    print(f"midi {f}, track {i, track.name}: 'ERROR'")
                    stop = True
                else:
                    break

        # print(f"midi {f} is monophonic" if monophonic else f"midi {f} is monophonic NOT !!")
        if not monophonic:
            print(f"midi {f} is monophonic NOT !!")
        # if monophonic:
        #     print(f"midi {f} is monophonic")
        if DETAILED and stop:
            break
