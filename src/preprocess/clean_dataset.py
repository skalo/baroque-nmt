# This is just a utility file to rename the midi files in the dataset and eventually, maybe, to remove them
import os
import glob

DATA_FOLDER = os.path.join('..', '..', 'data', 'baroque_large_trainset-cleaning')


def analysis(query):
    src = glob.glob(os.path.join(DATA_FOLDER, query))
    return sorted(src)


def _rename_without_overwriting(src, new_filename):
    dst = os.path.join(DATA_FOLDER, new_filename)
    i = 0
    while dst in os.listdir(DATA_FOLDER):
        tmp_filename = new_filename.split('.')[0] + '_(' + str(i) + ')' + new_filename.split('.')[1]
        dst = tmp_filename
    os.rename(src, dst)


def _substitution_logic(filename):
    """ This contains the logic for determining the new filename and will be modified for every different query"""
    code = filename.split("_")[2]
    BWV = str(int(code) + 598)
    return '_'.join(['bach', 'BWV', BWV, filename.split("_")[-1]])


def moving(src, act):
    """ To call only when you want to move stuff """

    for s in src:
        filename = os.path.basename(s)

        new_filename = _substitution_logic(filename)

        if act:
            confirm = input(
                "Do you want to actually move files? This should not overwrite files but it might be hard to revert anyway. [yn]")
            while confirm not in 'yYnN':
                confirm = input("Please type y or Y to overwrite files, n or N to abort the operation")
            if confirm in 'yY':
                _rename_without_overwriting(s, new_filename)
            else:
                print("Operation aborted, no files were moved.")


if __name__ == '__main__':
    query = 'bach-js_orgelbuechlein*.mid'
    src = analysis(query)
    act = True
    moving(src, act)
