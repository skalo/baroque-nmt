""" Step 4: Extract two-part polyphonic music from MIDI files """

import os
from collections import defaultdict
from itertools import combinations
from multiprocessing import Pool

from mido import MidiFile
import numpy as np

print(os.getcwd())

# from utils.decorators import timeit


timesig_counts = defaultdict(int)

TARGET_DIR = '../../data/original/baroque/deduped_midi'
TARGET_DIR_TWOPART = '../../data/baroque_large_trainset_twopart'

RELAX_FOR_TESTSET = True

REQUIRE_EXACTLY_TWO_PARTS = False

os.makedirs(TARGET_DIR_TWOPART, exist_ok=True)


def getNoteRangeAndTicks(mid):
    ticks = []
    notes = []
    num_notes_per_track = []
    time_sigs_per_track = []

    for i, track in enumerate(mid.tracks):
        num_ticks = 0
        num_time_sigs = 0
        num_notes = 0
        for msg in track:
            # if msg.type[:4] == 'note':
            #    notes.append(msg.note)
            #    num_ticks += int(msg.time)
            #    num_notes += 1
            # elif msg.type == 'time_signature':
            if msg.type == 'time_signature':
                num_time_sigs += 1

        # ticks.append(num_ticks)
        # num_notes_per_track.append(num_notes)
        time_sigs_per_track.append(num_time_sigs)
        timesig_counts[i] += num_time_sigs

        if not notes:
            notes = [0]
        if not ticks:
            ticks = [0]

    return min(notes), max(notes), max(ticks), num_notes_per_track, time_sigs_per_track


def is_monophonic(track, min_notes=0):
    """Returns True if MidiTrack is monophonic and has at least min_notes notes."""
    cur = 0
    cur_end_time = 0
    num_notes = 0

    notes = []

    # Look at each note-on. Find the corresponding note_off.
    for i, msg in enumerate(track):
        if msg.type == 'note_on' and msg.velocity > 0:
            if cur != 0:
                # Look for a note off at time-delta 0 for the current note before assuming overlap.
                overlap = True
                for j in range(i + 1, len(track)):
                    msg2 = track[j]
                    if msg2.time > 0:
                        # We moved ahead in time. It overlaps.
                        break
                    if msg2.type == 'note_off' or (msg2.type == 'note_on' and msg2.velocity == 0):
                        # no overlap
                        if msg2.note == cur:
                            overlap = False
                            break
                if overlap:
                    # print(f'prev note {cur} overlaps note {msg.note}')
                    return False
            cur = msg.note
            num_notes += 1
        elif msg.type == 'note_off' or (msg.type == 'note_on' and msg.velocity == 0):
            if msg.note == cur:
                cur = 0
    return num_notes >= min_notes


DEBUG = False
DEBUG_FILE = ''  # bach_motet_227_bwv-227_10_(c)icking-archive.mid' # 'bach-js_anna-magdalena_12_(nc)werths.mid'
DEBUG_TRACKS = [5]
MIN_NOTES_PER_PART = 10


def is_two_part(filepath):
    """Return False if not two-part. If it is, a list of indices of valid parts is returned."""
    try:
        midifile = MidiFile(os.path.join(TARGET_DIR, filepath))
    except:
        print(f'File {filepath} read failure; skipping.')
        return False

    monophonic_track_indices = []

    for i, track in enumerate(midifile.tracks):
        # Skip 1st track and last track; this seems to be valid for KunstDerFuge tracks.
        if not RELAX_FOR_TESTSET:
            if i == 0 or i == len(midifile.tracks) - 1:
                continue
        if is_monophonic(track, min_notes=MIN_NOTES_PER_PART):
            if DEBUG:
                print(f'Monophonic: Track {i} in file {filepath}')
            monophonic_track_indices.append(i)
        elif DEBUG:
            print(f'*** NON- Monophonic: Track {i} in file {filepath}')

    if not RELAX_FOR_TESTSET:
        # Require at least 2 parts.
        if len(monophonic_track_indices) < 2:
            return False

        # Require exactly 2 parts if specified.
        if REQUIRE_EXACTLY_TWO_PARTS and len(monophonic_track_indices) > 2:
            return False

    # TODO: Additional conditions.

    return monophonic_track_indices


def is_metadata_track(track):
    for i, msg in enumerate(track):
        if msg.type == 'note_on' and msg.velocity > 0:
            return False
    return True


def get_avg_pitch_height(track):
    pitches = []
    for msg in track:
        if msg.type == 'note_on' and msg.velocity > 0:
            pitches.append(msg.note)
    return np.mean(pitches)


def write_twopart_midi(input_filepath, track_idxs):
    try:
        midifile = MidiFile(os.path.join(TARGET_DIR, input_filepath))
    except:
        print(f'File {input_filepath} read failure; skipping.')
        return False

    for i, j in combinations(track_idxs, 2):
        new_midi = MidiFile(ticks_per_beat=midifile.ticks_per_beat)
        # Start by adding in track 0 if it's a metadata track. This is the case for kunstderfuge tracks.
        track = midifile.tracks[0]
        track0_ismeta = is_metadata_track(track)
        if track0_ismeta:
            new_midi.tracks.append(track)

        # loop over the 2 tracks, and figure out which one is the "right hand" on average.
        tracks_to_append = []

        for track_idx in [i, j]:  # include the 2 tracks.
            # Only include track 0 here if it's not metadata track, since i and j are supposed to be part indices and we don't want to duplicate track 0.
            track = midifile.tracks[track_idx]
            if track_idx == 0 and track0_ismeta:
                continue
            tracks_to_append.append((get_avg_pitch_height(track), track, track_idx))
        #print([x[0] for x in sorted(tracks_to_append, key=lambda x: x[0], reverse=True)])

        # Sort the tracks in descending order of avg midi pitch height, and add to output file.
        sorted_track_indices = []
        for _, track, track_idx in sorted(tracks_to_append, key=lambda x: x[0], reverse=True):
            new_midi.tracks.append(track)
            sorted_track_indices.append(track_idx)

        outpath = os.path.join(TARGET_DIR_TWOPART, input_filepath)
        p, ext = os.path.splitext(outpath)
        sorted_i = sorted_track_indices[0]
        sorted_j = sorted_track_indices[1]
        new_outpath = f'{p}_{sorted_i}_{sorted_j}{ext}'  # include part #s
        new_midi.save(new_outpath)


def main():
    files = os.listdir(TARGET_DIR)

    for i, f in enumerate(files):
        if DEBUG_FILE and f != DEBUG_FILE:
            continue
        # try:
        if True:
            print(i, f)
            filename = os.path.join(TARGET_DIR, f)
            print(f'Reading file {filename}')
            mid = MidiFile(filename)
        # except:
        #    print(f'File {f} read failure; skipping.')
        #    continue
        if i % 100 == 0:
            print(i)

        if DEBUG_FILE:
            print(f'File {i}: {f}')
            print(f'# tracks: {len(mid.tracks)}')
            for track in mid.tracks:
                print(track)

            # for msg in mid.tracks[0]:
            #     if msg.type[:4] != 'note':
            #         print(msg.type, msg.time ) #== 'time_signature':)

            for j in DEBUG_TRACKS:
                print(f'\nTRACK {j}')
                for msg in mid.tracks[j]:
                    print(msg)

        # min_n, max_n, max_t, num_notes, time_sigs = getNoteRangeAndTicks(mid)

        # for i in range (1, len(time_sigs)):
        #    if time_sigs[i] > 0:
        #        print(f'{time_sigs[i]} time sig(s) in track {i} for file {f}')

        # print(min_n, max_n, max_t, num_notes, time_sigs)

        mono_indices = is_two_part(mid)

        if not mono_indices:
            continue

        print(f'>= 2 part file found: {f} with tracks {mono_indices}')

    # print('time_sigs per track:')
    # pprint(timesig_counts)

    print('\nDONE!')


# @timeit
def get_good_files(filepaths):
    p = Pool(os.cpu_count())
    result = p.map(is_two_part, files)

    return [(f, idxs) for f, idxs in zip(filepaths, result) if idxs]


# @timeit
def write_twopart_midi_pool(files_with_indices):
    p = Pool(os.cpu_count())
    result = p.starmap(write_twopart_midi, files_with_indices)


if __name__ == '__main__':
    # main()

    files = os.listdir(TARGET_DIR)
    good_files_track_indices = get_good_files(files)

    # pprint(list(good))
    print(len(list(good_files_track_indices)))

    # print(list(good[0]))
    # write_twopart_midi(*list(good[0]))

    write_twopart_midi_pool(good_files_track_indices)
