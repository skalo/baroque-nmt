""" Step 3: Remove repeated files from the dataset """

from glob import glob
import os
from shutil import copyfile


SRC_DIR = '../data/original/baroque/midi'
TARGET_DIR = '../data/original/baroque/deduped_midi'


def get_stem(filename):
    return filename.split('(c)')[0]


if __name__ == '__main__':
    files = glob(os.path.join(SRC_DIR, '*mid'))

    skipped_files = []

    copy_files = []
    filename_stems = set()

    for f in files:
        stem = get_stem(f)
        if stem in filename_stems:
            # Arbitrarily skip any versions of the file after the first one we find.
            skipped_files.append(f)
            continue
        filename_stems.add(stem)
        copy_files.append(f)

    N = len(copy_files)

    cwd = os.getcwd()

    for i, f in enumerate(copy_files):
        name = os.path.basename(f)
        print(f'{i}/{N}: Copying file {name}')

        src = os.path.join(cwd, f)
        target = os.path.join(cwd, TARGET_DIR, name)
        copyfile(src, target)

    print(f'Skipped files: {skipped_files}')
    print(f'{len(files)} files selected')
    print(f'Skipping {len(files) - N} files.')
