""" Step 2: Extract files from the downloaded dataset after selection with pick_folders """
import os
from shutil import copyfile

from pick_folders import pick_folders, pick_midi_files


SRC_DIR = '/data/baroque'  # Update src MIDI directory here.

TARGET_DIR = '../../data/original/baroque/midi'

os.makedirs(TARGET_DIR, exist_ok=True)

if __name__ == '__main__':
    dirs = pick_folders(rootdir=SRC_DIR)
    files = pick_midi_files(dirs)
    print(f'{len(files)} files selected')

    cwd = os.getcwd()

    for f in files:
        name = os.path.basename(f)
        print(f'Copying file {name}')

        src = os.path.join(cwd, SRC_DIR, f)
        target = os.path.join(cwd, TARGET_DIR, name)
        copyfile(src, target)