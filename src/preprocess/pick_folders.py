""" Step 1: Pick folders to use based on a list of whitelisted composers.

Ignores paths with "!" chars, like "!live!", except for "!various_composers!"
"""

import os
from pprint import pprint

ROOTDIR = '/data/baroque'


def _get_dirlist(rootdir):
    dirlist = []

    with os.scandir(rootdir) as rit:
        for entry in rit:
            if not entry.name.startswith('.') and entry.is_dir():
                dirlist.append(entry.path)
                dirlist.extend(_get_dirlist(entry.path))

    dirlist.sort()  # Optional, in case you want sorted directory names
    return dirlist


def pick_folders(rootdir, baroque_only=False):
    if baroque_only:
        composers = set()
        with open('baroque_composers.txt') as f:
            for line in f:
                composers.add(line.strip().lower())

    dirs = _get_dirlist(rootdir)
    n = len(rootdir)
    dirs = [d[n:] for d in dirs]

    VARIOUS = '/!various-composers!'

    good_dirs = set()

    for d in dirs:
        if d.startswith(VARIOUS):
            start = d[len(VARIOUS) + 1:]
        else:
            start = d[1:]

        if not start or '!' in start:
            continue
        name = start.split('/')[0]
        # print (d, name)
        if baroque_only:
            for c in composers:
                if c in name or name in c:
                    good_dirs.add(d[1:])
        else:
            good_dirs.add(d[1:])
    return good_dirs


def pick_midi_files(dirs):
    # get midi files from dirs.
    files = []
    for d in dirs:
        midis = [os.path.join(d, f) for f in os.listdir(os.path.join(ROOTDIR, d))]
        files.extend(midis)

    files = [f for f in files if f.endswith('.mid')]

    return files


if __name__ == '__main__':
    dirs = pick_folders(rootdir=ROOTDIR)
    # pprint (dirs)
    # pprint (len(dirs))

    files = pick_midi_files(dirs)
    # pprint([f for f in files if 'invention' in f])

    pprint(files)
    print(f'{len(files)} files selected')
