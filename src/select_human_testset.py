# Select exactly 100 items from testset to use for human subjects.
# Reads from TRAIN_FOLDER; writes to VALIDATION_FOLDER

import glob
import numpy as np
import os
import random

from config import TRAIN_FOLDER, VALIDATION_FOLDER, NUM_SEGMENTS_HUMAN_EVAL

np.random.seed(42)

with open(os.path.join(TRAIN_FOLDER, 'all_input.txt')) as f:
    inputs = f.readlines()

with open(os.path.join(TRAIN_FOLDER, 'all_output.txt')) as f:
    outputs = f.readlines()


idxs = np.random.choice(len(inputs), NUM_SEGMENTS_HUMAN_EVAL, replace=False)

with open(os.path.join(VALIDATION_FOLDER, 'all_input.txt'), 'w') as f_input:
    with open(os.path.join(VALIDATION_FOLDER, 'all_output.txt'), 'w') as f_output:
        for i in idxs:
            f_input.write(inputs[i])
            f_output.write(outputs[i])

print('Done.')
