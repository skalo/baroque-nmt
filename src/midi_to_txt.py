from collections import defaultdict
import glob
import os
from pprint import pprint
import re
import shutil
from functools import partial
from itertools import chain
from multiprocessing import Pool

import numpy as np
from music21 import converter
from music21.exceptions21 import StreamException
from music21.meter import TimeSignature
from music21.meter import MeterException

from config import CHUNK_SIZE, TXT_FOLDER, MIDI_FOLDER, TRAIN_FOLDER, VALIDATION_FOLDER, SIG_DIGITS
from config import NULL_POSITION_TOKEN, TRAIN_TEST_SPLIT, BEAT_POSITION_MOD_MEASURE_LENGTH
from config import MAKE_CHUNKS_FROM_ALL_MEASURE_STARTS

zero_count = 0
MIN_NUM_NOTES = 10  # each part must have this many notes or we skip it.


def train_test_split(files, percent_train):
    N = len(files)
    num_train = int(N * percent_train)
    num_test = N - num_train
    split = [0] * num_train + [1] * num_test
    np.random.seed(42)
    np.random.shuffle(split)

    return [f for f, c in zip(files, split) if c == 0], [f for f, c in zip(files, split) if c == 1]


def convert_midi_to_txt_chunks(file, output_directory=None):
    """
    Given a MIDI file, transform the musical information into a series of texts file.
    The MIDI file is separated in chunks of given length in terms of number of measures and a single text file is
    created for each of these chunks.
    Only information about notes and rests are kept. The information on each of these events is split in three parts:
     - beat position, saying how many beats have passed since the beginning of a chunk
     - pitch, encoded in MIDI numbers, with 0 being used for rests
     - duration, in beat units

    :param file:
    :param output_directory:
    :return:
    """
    global zero_count
    print("converting {}".format(file.split('/')[-1]))
    try:
        piece = converter.parse(file, quantizePost=True, quarterLengthDivisors=(8, 6))
    except ZeroDivisionError as e:
        print(f"Can't parse {file}. Got division by 0 error in music21 probably from mspq==0 in midiEventsToTempo")
        return
    except MeterException as e:
        # Music21 sucks ass.
        print(f"Can't parse {file}. Got MeterException in music21")
        return
    except TypeError as e:
        # Music21 sucks even more ass. Bug in music21/midi/translate.py line 1973, in midiFilePathToStream
        print(f"Music21 sucks. Can't parse {file}. got TypeError: {e}")
        return

    # Fix missing TimeSignature elements in other parts -- this is a workaround for a Music21 bug.
    time_sigs = piece[0].flat.getElementsByClass(TimeSignature)
    for ts in time_sigs:  # Is there always one element in this?
        chunk_length = ts.beatCount * ts.beatDuration.quarterLength
        offset = ts.getOffsetBySite(piece[0])
        for i, part in enumerate(piece):
            if i == 0:  # Skip first part
                continue
            part.insert(offset, ts)

    # Find the max measure numbers.
    try:
        [part.makeMeasures(inPlace=True) for part in piece]
    except StreamException as e:
        print(f'Caught exception music21.exceptions21.StreamException. Skipping piece {file}')
        return

    max_measure = max([len(part) for part in piece]) // CHUNK_SIZE * CHUNK_SIZE  # drop extra measures at the end
    if MAKE_CHUNKS_FROM_ALL_MEASURE_STARTS:
        chunks = [(i, i + CHUNK_SIZE) for i in range(max_measure)]
    else:
        chunks = [(i, i + CHUNK_SIZE) for i in range(0, max_measure, CHUNK_SIZE)]
    # midi.realtime.StreamPlayer(piece).play()

    # Store a short list for every "chunk" of the file. The list should contain two elements: one for 
    # each of the two parts. This is necessary because we got through part 1 first, and then part 2.
    # The dict key is (start, end) measure indices; value is a list of tuples of (filename, text) for eacch part.
    textfile_pairs_to_write = defaultdict(list)
    already_warned = False
    for i, part in enumerate(piece):
        # Write midi information as txt while scrolling measures
        track_name = os.path.join(output_directory, os.path.basename(file) + '_part_%d' % i)
        for start, end in chunks:
            notes_txt = []
            beat_pos = 0
            for measure in part[start: end]:
                measure_len_quarters = measure.quarterLength
                for event in measure.flat:  # get list of notes (flatten out subvoices etc)

                    # beat position: turn decimal point into a "z", and surround with "b"s for "beat position"
                    beat_pos_str = 'x' if NULL_POSITION_TOKEN else 'b' + f'{beat_pos:.4f}'.replace('.', 'z') + 'b'

                    # note pitch (or rest token)
                    if getattr(event, 'isNote', None) and event.isNote:  # event.isNote not defined for some events
                        pitch_string = str(event.pitch.midi)
                    elif getattr(event, 'isRest', None) and event.isRest:
                        pitch_string = 0
                    else:  # if the event is neither a note or a rest, skip it
                        continue

                    # duration
                    if event.quarterLength == 0:  # skip zero-length events and keep track of their amount
                        zero_count += 1
                        continue
                    else:  # turn decimal point into a "z", fraction into a "v" and surround with "d"s for "duration"
                        if '/' in str(event.quarterLength):
                            duration_token = 'D' + f'{event.quarterLength}'.replace('.', 'z').replace('/', 'v') + 'D'
                        else:     
                            duration_token = 'D' + f'{event.quarterLength:.4f}'.replace('.', 'z').replace('/', 'v') + 'D'

                    notes_txt.append(f' {beat_pos_str} {pitch_string} {duration_token}')

                    if BEAT_POSITION_MOD_MEASURE_LENGTH:
                        beat_pos = round(beat_pos + float(event.quarterLength), SIG_DIGITS)
                        while beat_pos >= measure_len_quarters - 1e-5:
                            beat_pos -= measure_len_quarters
                        beat_pos = max(0, round(beat_pos, SIG_DIGITS))
                    else:
                        beat_pos = round(beat_pos + float(event.quarterLength), SIG_DIGITS)
                    if beat_pos > 100 and not already_warned:
                        print(f"**** BEAT POSITION GOT TOO BIG! **** {track_name + '_%d-%d.txt' % (start, end)}")
                        already_warned = True
            filename = track_name + '_%d-%d.txt' % (start, end)
            notes_txt = ''.join(chain.from_iterable(notes_txt)).strip()
            textfile_pairs_to_write[(start, end)].append((filename, str(notes_txt)))

    # Make sure the example is interesting enough, for ALL parts. Otherwise write none of them.
    for _, pair_list in textfile_pairs_to_write.items():
        for filename, txt in pair_list:
            if len(txt.split()) < MIN_NUM_NOTES * 3:
                # Too boring. Skip.
                break
        # for...else: Only if neither part was too boring, write both parts for this chunk. 
        else:
            for filename, txt in pair_list:
                with open(filename, "w") as f:
                    f.write(txt)


def tokenize_files(files, output_folder):
    p = Pool(os.cpu_count())
    # p = Pool(1)
    fn = partial(convert_midi_to_txt_chunks, output_directory=output_folder)
    p.map(fn, files)


# REGEX to grab filename before the "_<part1>_<part2>.mid" ending of a two-part midi file.
REGEX_MIDI_FILE_STEM = re.compile('(?P<stem>.+)_\d+_\d+\.mid')  # e.g. return "stem" for "stem_2_3.mid"


def get_file_stem(filename):
    base = os.path.basename(filename)
    m = REGEX_MIDI_FILE_STEM.match(base)
    return m.groups()[0]


if __name__ == '__main__':
    midi_files = glob.glob(os.path.join(MIDI_FOLDER, '*.mid'))
    # print(midi_files)
    print(MIDI_FOLDER)
    
    # N.B. This was causing leakage. Train/test split isn't taking into account common parent midifiles
    # when extracting 2-part pairs from multiple parts.
    train_files, test_files = train_test_split(midi_files, TRAIN_TEST_SPLIT)  # 0.8 #0.99

    print(f'Before filtering, train size = {len(train_files)}, test size = {len(test_files)}')

    # Temporary solution to leakage problem: go thorugh train/test split and move all leakage instances to
    # test_files. Assumes test_files was overly small; this increases its size a bit.
    test_stems = set(get_file_stem(f) for f in test_files)
    to_remove = []
    for f in train_files:
        if get_file_stem(f) in test_stems:
            to_remove.append(f)
            test_files.append(f)
            # print(f'Moved file {os.path.basename(f)}')
    train_files = list(filter(lambda f: f not in to_remove, train_files))

    #pprint(test_stems)

    print(f'After filtering, train size = {len(train_files)}, test size = {len(test_files)}')

    train_txt_folder = TRAIN_FOLDER
    val_txt_folder = VALIDATION_FOLDER

    # Delete Train/Validation folders.
    shutil.rmtree(TRAIN_FOLDER)
    shutil.rmtree(VALIDATION_FOLDER)

    os.makedirs(train_txt_folder, exist_ok=False)
    os.makedirs(val_txt_folder, exist_ok=False)

    tokenize_files(train_files, train_txt_folder)
    tokenize_files(test_files, val_txt_folder)

    print('Done.\n')
    print(f'Zero-length duration events detected: {zero_count}')
    print(f'Done converting midi to text. Results in {TXT_FOLDER}')
