now=`date '+%Y_%m_%d__%H_%M_%S'`;
mkdir -p experiments/$now
cp *pt experiments/$now
cp data/*pt experiments/$now
cp data/*txt experiments/$now
cp pred-transformer*.txt experiments/$now
cp midi experiments/$now
cp *sh experiments/$now
