import os
from random import sample

K = 100 # 500  # of lines to sample

BASE = '/home/eric/git/question-answering_music_gen/outputs'

#DIR1 = os.path.join(BASE, '20190709_4bar_mod/data')
#DIR2 = os.path.join(BASE, '20190709_4bar/data')


DIR1 = os.path.join(BASE, '20200525_000652_transformer_lr=10_d=0.1_ls=0.1_l=6_wv=512_ff=2048_h=16_o=adam_batchsz=4096_/data')
#DIR2 = os.path.join(BASE, '20190827_215333_transformer_lr=10_d=0.1_ls=0.1_l=6_wv=512_ff=2048_h=16_o=adam_batchsz=4096_/data')

dirs = [DIR1]

#dirs = [DIR1, DIR2]

files = ['filenames_val.txt', 'src_val.txt', 'tgt_val.txt']

# Verify all files have same length before trying to sample.
lens = []
for dir in dirs:
    for filename in files:
        with open(os.path.join(dir, filename), 'r') as f:
            lines = f.readlines()
            lens.append(len(lines))

N = lens[0]

print(f'Sampling {K} lines from files of length {N}')
assert K <= N, 'Too few lines to sample'

for i in range(1, len(lens)):
    assert lens[i] == N

# Now make a shuffling.
idxs = sample(range(N), K)
print(idxs)

# Shuffle and output *_shuf versions of all files.
for dir in dirs:
    for filename in files:
        in_filepath = os.path.join(dir, filename)
        with open(in_filepath, 'r') as f:
            lines = f.readlines()
            lines_out = [lines[i] for i in idxs]

            root, ext = os.path.splitext(in_filepath)
            out_filepath = f'{root}_shuf{ext}'
            with open(out_filepath, 'w') as f_out:
                print(f'Writing file {out_filepath}')
                f_out.writelines(lines_out)

print('Done!')
