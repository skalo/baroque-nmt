#!/bin/bash

# USAGE:
#
# Train, Generate, and make MIDI files:
# - train_and_generate.sh 
#
# Generate only:  (note that MODEL_DIR is relative to ../../outputs/)
# - train_and_generate.sh gen <MODEL_DIR> <MODEL_STEP>
#
# MIDI only:
# - train_and_generate.sh midi <MODEL_DIR> <MODEL_STEP>

if [[ "$1" = "gen" ]]; then
  GENERATE_ONLY=true
else
  GENERATE_ONLY=false
fi

if [[ "$1" = "midi" ]]; then
  MIDI_ONLY=true
else
  MIDI_ONLY=false
fi


###################################

export CUDA_VISIBLE_DEVICES=0
gpus_to_use=0
num_gpus=1
debug_level=0

now=`date +%Y%m%d_%H%M%S`
###################################

# OPTIONS

USE_TESTSET=false  # Use the small testset for train/test, for speed.

WAIT_FOR_DEBUGGER=false  # Wait for the VSCode debugger to attach to the training script.

REL_BEAT_POSITION_ENCODING=false  # If true, use beat position in encoder. If false, use regular transformer encoding.

PREDICT_TOP=false  # if false, model predicts the bottom part given the top part.


if $WAIT_FOR_DEBUGGER ; then
  WAIT_FOR_DEBUGGER_FLAG="--wait_for_attach"
else
  WAIT_FOR_DEBUGGER_FLAG=" "
fi

if $PREDICT_TOP ; then
  # Predict Top
  SRC_PART_TEXT=all_output.txt
  TGT_PART_TEXT=all_input.txt
  invert_parts_flag="--invert_parts"
else
  # Predict Bottom
  SRC_PART_TEXT=all_input.txt
  TGT_PART_TEXT=all_output.txt
  invert_parts_flag=""
fi

if $REL_BEAT_POSITION_ENCODING ; then
  POS_ENCODING_FLAG="--beat_position_encoding"
else
  POS_ENCODING_FLAG="--position_encoding"
fi

# PATHS

if $USE_TESTSET ; then
  RELATIVE_SRC_DATA_DIR=../../data/baroque_testset_twopart/augmented
  REL_TENSORBOARD_LOG_DIR=../../outputs/tensorboard
else
  #RELATIVE_SRC_DATA_DIR=../../data/kunst_subset/augmented
  RELATIVE_SRC_DATA_DIR=../../data/clean_dataset/all/twopart_midi/augmented
  REL_TENSORBOARD_LOG_DIR=../../outputs/tensorboard
fi

# TRAINING PARAMS
processed_data_name=twopart
#model_type=transformer  #seq2seq
model_type=transformer  #seq2seq


train_steps=100000 # 5000 50000
warmup_steps=8000
valid_steps=500
save_checkpoint_steps=500

learning_rate=10 #2
dropout=0.1
label_smoothing=0.1
batch_size=4096 # 8192

transformer_layers=6 #72 # 6
word_vec_size=512
transformer_ff_size=2048
num_heads=16 # 8, 16, 32

optimizer=adam

# GENERATE PARAMS
generate_batch_size=40  #20 #10 #40
beam_size=20

###################################
# Derived Paths
SRC_DATA_DIR=../$RELATIVE_SRC_DATA_DIR

if $MIDI_ONLY || $GENERATE_ONLY ; then
  EXPT_NAME=$2  # Get path from comandline
  train_steps=$3
else
  EXPT_NAME=${now}_${model_type}_lr=${learning_rate}_d=${dropout}_ls=${label_smoothing}_l=${transformer_layers}_wv=${word_vec_size}_ff=${transformer_ff_size}_h=${num_heads}_o=${optimizer}_batchsz=${batch_size}_${invert_parts_flag}
fi

RELATIVE_OUTPUT_DIR=../../outputs/$EXPT_NAME
OUTPUT_DIR=../$RELATIVE_OUTPUT_DIR  # Go back up one becuase we're going to cd down a level.
MIDI_DIR=$OUTPUT_DIR/midi
data_folder=$OUTPUT_DIR/data  # where we store transformed data ready for OpenNMT
model_path=$OUTPUT_DIR/${model_type}-model
txt_output=${OUTPUT_DIR}/pred-$model_type.txt
TENSORBOARD_LOG_DIR=../$REL_TENSORBOARD_LOG_DIR
###################################

pushd .
cd OpenNMT-py
mkdir -p $MIDI_DIR/
mkdir -p $data_folder/


#if [[ ! ( $MIDI_ONLY || $GENERATE_ONLY ) ]] ; then
if ! ( $MIDI_ONLY || $GENERATE_ONLY ) ; then
  # PREPARE DATA
  echo "PREPARE DATA"
  cp $SRC_DATA_DIR/training_set/$SRC_PART_TEXT $data_folder/src_train.txt
  cp $SRC_DATA_DIR/training_set/$TGT_PART_TEXT $data_folder/tgt_train.txt
  cp $SRC_DATA_DIR/validation_set/$SRC_PART_TEXT $data_folder/src_val.txt
  cp $SRC_DATA_DIR/validation_set/$TGT_PART_TEXT $data_folder/tgt_val.txt
  cp $SRC_DATA_DIR/validation_set/all_src_filenames.txt $data_folder/filenames_val.txt
  
  python preprocess.py -train_src $data_folder/src_train.txt \
                       -train_tgt $data_folder/tgt_train.txt \
                       -valid_src $data_folder/src_val.txt \
                       -valid_tgt $data_folder/tgt_val.txt \
                       -save_data $data_folder/twopart \
                       -shard_size 2000000
  
  
  # TRAIN
  echo "TRAIN"
  python train.py -data $data_folder/$processed_data_name -save_model $model_path \
          -layers $transformer_layers -rnn_size $word_vec_size -word_vec_size $word_vec_size \
          -transformer_ff $transformer_ff_size -heads $num_heads  \
          -encoder_type $model_type -decoder_type $model_type \
          $POS_ENCODING_FLAG \
          -train_steps $train_steps -max_generator_batches 2 -dropout $dropout \
          -batch_size $batch_size -batch_type tokens -normalization tokens -accum_count 2 \
          -optim $optimizer -adam_beta2 0.998 -decay_method noam \
          -warmup_steps $warmup_steps -learning_rate $learning_rate \
          -max_grad_norm 0 -param_init 0 -param_init_glorot \
          -label_smoothing $label_smoothing \
          -valid_steps $valid_steps -save_checkpoint_steps $save_checkpoint_steps \
          -world_size $num_gpus -gpu_ranks $gpus_to_use \
          -gpu_verbose_level $debug_level \
          -tensorboard -tensorboard_log_dir $TENSORBOARD_LOG_DIR \
          $WAIT_FOR_DEBUGGER_FLAG
fi

if $GENERATE_ONLY ; then
  # INFERENCE
  
  #echo "SHUFFLE DATA"
  # Shuffles the inputs as well as the expected outputs and the source filenames, for debugging/reference.
  # All three files are shuffled in the same manner due to the "tee" command on the souce of randomness.
  # See https://unix.stackexchange.com/questions/220390/shuffle-two-parallel-text-files
  #rm onerandom tworandom threerandom
  #mkfifo onerandom tworandom threerandom
  #tee onerandom tworandom threerandom < /dev/urandom > /dev/null &
  #shuf --random-source=onerandom $data_folder/src_val.txt > $data_folder/src_val_shuf.txt &
  #shuf --random-source=tworandom $data_folder/tgt_val.txt > $data_folder/tgt_val_shuf.txt &
  #shuf --random-source=threerandom $data_folder/filenames_val.txt > $data_folder/filenames_val_shuf.txt &
  #wait
  #rm onerandom tworandom threerandom

  # Now we require pre-shuffled data based on shuffle_data.py

  echo "GENERATE"

  python translate.py -model ${model_path}_step_${train_steps}.pt \
    -src $data_folder/src_val_shuf.txt \
    -output $txt_output -replace_unk -verbose \
    -gpu $gpus_to_use -batch_size $generate_batch_size \
    -beam_size $beam_size
fi 


if $MIDI_ONLY ; then
  # GENERATE MIDI
  echo "OUTPUT TO MIDI"
  
  python ../make_2_part_scores_from_predictions.py \
    -t $data_folder/src_val_shuf.txt \
    -b $txt_output \
    -n $model_type \
    -o $MIDI_DIR \
    $invert_parts_flag
  
  echo "MIDI generated"
fi

popd

echo "DONE!"
