#!/usr/bin/env bash
export CUDA_VISIBLE_DEVICES=0
# python train.py -data data/twopart -save_model twopart-model -world_size 1 -gpu_ranks 0 -valid_steps 20 -save_checkpoint_steps 20

# batch_size 4096
#        -layers 6 -rnn_size 512 -word_vec_size 512 -transformer_ff 2048 -heads 8  \

python  train.py -data data/twopart -save_model transformer-model \
        -layers 6 -rnn_size 512 -word_vec_size 512 -transformer_ff 2048 -heads 8  \
        -encoder_type transformer -decoder_type transformer -position_encoding \
        -train_steps 100000  -max_generator_batches 2 -dropout 0.1 \
        -batch_size 4096 -batch_type tokens -normalization tokens  -accum_count 2 \
        -optim adam -adam_beta2 0.998 -decay_method noam -warmup_steps 8000 -learning_rate 2 \
        -max_grad_norm 0 -param_init 0  -param_init_glorot \
        -label_smoothing 0.1 -valid_steps 500 -save_checkpoint_steps 500 \
        -world_size 1 -gpu_ranks 0
        #-gpu_verbose_level 5


# orig: 6 layers, 8 heads
    # Prev 49500 good experiment: 8 layers, 16 heads.

# TRANSFORMER
#python  train.py -data data/twopart -save_model transformer-model \
#        -layers 8 -rnn_size 512 -word_vec_size 512 -transformer_ff 4096 -heads 32  \
#        -encoder_type transformer -decoder_type transformer -position_encoding \
#        -train_steps 100000  -max_generator_batches 2 -dropout 0.1 \
#        -batch_size 8192 -batch_type tokens -normalization tokens  -accum_count 2 \
#        -optim adam -adam_beta2 0.998 -decay_method noam -warmup_steps 8000 -learning_rate 2 \
#        -max_grad_norm 0 -param_init 0  -param_init_glorot \
#        -label_smoothing 0.1 -valid_steps 500 -save_checkpoint_steps 500 \
#        -world_size 1 -gpu_ranks 0
#        #-gpu_verbose_level 5

# SEQ2SEQ
#python  train.py -data data/twopart -save_model seq2seq-model \
#        -encoder_type brnn \
#        -layers 6 -rnn_size 512 -word_vec_size 512  \
#        -train_steps 100000  -max_generator_batches 2 -dropout 0.1 \
#        -batch_size 4096 -batch_type tokens -normalization tokens  -accum_count 2 \
#        -optim adam -adam_beta2 0.998 -decay_method noam -warmup_steps 8000 -learning_rate 2 \
#        -max_grad_norm 0 -param_init 0  -param_init_glorot \
#        -label_smoothing 0.1 -valid_steps 500 -save_checkpoint_steps 500 \
#        -world_size 1 -gpu_ranks 0


#warmup steps 8000

#python  train.py -data data/twopart -save_model transformer-model \
#        -layers 3 -rnn_size 256 -word_vec_size 256 -transformer_ff 512 -heads 16  \
#        -encoder_type transformer -decoder_type transformer -position_encoding \
#        -train_steps 100000  -max_generator_batches 2 -dropout 0.3 \
#        -batch_size 512 -batch_type tokens -normalization tokens  -accum_count 2 \
#        -optim adam -adam_beta2 0.998 -decay_method noam -warmup_steps 8000 -learning_rate 2 \
#        -max_grad_norm 0 -param_init 0  -param_init_glorot \
#        -label_smoothing 0.1 -valid_steps 200 -save_checkpoint_steps 200 \
#        -world_size 1 -gpu_ranks 0





