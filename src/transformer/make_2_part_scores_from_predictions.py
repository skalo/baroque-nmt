# Set up music21 as follows
# set it up to find MuseScore:
#
# us = environment.UserSettings()
# us['musescoreDirectPNGPath'] = '/usr/bin/musescore3' or "/Applications/MuseScore 2.app/Contents/MacOS/mscore" (or wherever you have it) –

import argparse
from multiprocessing import Pool
import os
import pickle
import random

from music21.stream import Stream, Part, Score
from music21.note import Note, Rest
from music21.duration import Duration
import music21.instrument as instrument
from music21.midi.translate import streamToMidiFile
import numpy as np


MIDI_TO_PITCH = ['C', 'D-', 'D', 'E-', 'E', 'F', 'F#', 'G', 'A-', 'A', 'B-', 'B']


def midi_to_note(midi):
    pitch = MIDI_TO_PITCH[midi % 12]
    octave = midi // 12 - 1
    return pitch + str(octave)


def dur_tok_to_float(tok):
    assert tok and tok[0] == 'D'

    dur = tok.strip('D')
    dur = dur.replace('z', '.').replace('v', '/')
    dur = float(eval(dur))
    return dur


def notes_to_part(notes, volume=80, instrument_obj=None):
    p = Part()
    #print(notes)

    midi_list = []

    # Get pairs of MIDI, duration.
    for i, tok in enumerate(notes):
        if i%3 == 0:
            # Beat counter thing.
            continue
        if i%3 == 1:
            # MIDI.
            try:
                midi = int(tok)
            except ValueError:
                # Network is behaving badly and output an illegal token.
                print(f'Illegal token output in MIDI: {tok}')
                #print('NOTES:', notes)
                return p
            continue  # nothing to do until we get duration on next pass

        # DURATION
        if tok[0] == 'D':
            dur = dur_tok_to_float(tok)
            
        else: # Network is behaving badly and output an illegal token.
            print(f'Illegal token output in duration: {tok}')
            return p

        # Add the note to the score.
        if midi==0:
            n = Rest(quarterLength=dur)    
        else:
            n = Note(nameWithOctave=midi_to_note(midi), quarterLength=dur)
            n.volume.velocity = volume
            midi_list.append(midi)
        p.append(n)


    # Determine instrument to use, unless specified.
    if not instrument_obj:
        if midi_list and np.mean(midi_list) < 60:
            instrument_obj = instrument.Bassoon()
        else:
            instrument_obj = instrument.Oboe()
    p.insert(0, instrument_obj)

    return p


def notes_to_score(notes1, notes2):
    p1 = notes_to_part(notes1)  # auto-select instrument for part 1
    p2 = notes_to_part(notes2, volume=90, instrument_obj=instrument.Piano())  # force piano for part 2
    sc = Score()
    sc.insert(0, p1)
    sc.insert(0, p2)
    #print(p1,p2)
    return sc


def gen_all():
    for i, line1 in enumerate(TOP_LINES):
        try:
            line2 = BOTTOM_LINES[i]
        except:
            print(i, 'ERROR in line', line2)
            break
    notes1 = line1.split()
    notes2 = line2.split()
    #print(notes1)
    #print(notes2)
    if i % 100 == 0:
            print(f'{i}/{len(TOP_LINES)}')
    sc = notes_to_score(notes1, notes2)
    #print(sc)
    #sc.show()
    mf = streamToMidiFile(sc)
    mf.open(f'{OUTPUT_DIR}/{MODEL_NAME}{i:03d}.mid', 'wb')
    mf.write()
    mf.close()
    print('DONE!')


def generate_midi_single(i):
    line1 = TOP_LINES[i]
    try:
        line2 = BOTTOM_LINES[i]
    except:
        print(i, 'ERROR in bottom line')
        return
    notes1 = line1.split()
    notes2 = line2.split()

    if i % 50 == 0:
            print(f'{i}/{NUMBER_TO_GENERATE}')
    sc = notes_to_score(notes1, notes2)

    mf = streamToMidiFile(sc)
    mf.open(f'{OUTPUT_DIR}/{MODEL_NAME}{i:03d}.mid', 'wb')
    mf.write()
    mf.close()


def parse_args():
    parser = argparse.ArgumentParser(description='Convert pair of input/output text representation files to MIDI files.')
    parser.add_argument('--top_part', '-t',
                        help='Path to text file giving the top music part in the score')
    parser.add_argument('--bottom_part', '-b',
                        help='Path to text file giving the bottom music part in the score')
    parser.add_argument('--name', '-n',
                        help='Prefix of the MIDI file names to generate')
    parser.add_argument('--output_dir', '-o',
                        help='Output directory path for MIDI files')
    parser.add_argument('--number_to_generate', '-g',
                        help='Number of outputs to generate (default is to generate all)')
    parser.add_argument('--invert_parts', action='store_true',
                        help='Invert parts in output so that the given top part is on bottom and the bottom part is on top.')

    return parser.parse_args()


if __name__ == '__main__':
    global MODEL_NAME, NUMBER_TO_GENERATE, TOP_LINES, BOTTOM_LINES, OUTPUT_DIR

    args = parse_args()

    MODEL_NAME = args.name
    OUTPUT_DIR = args.output_dir

    #INPUT_FILE = '../../../data/baroque_testset_twopart/validation_set/all_input.txt'
    #INPUT_FILE = 'data/src_val.txt'

    #PRED_FILE = 'pred-transformer_test_val.txt'
    #PRED_FILE = '../../../data/baroque_testset_twopart/validation_set/all_output.txt'

    top = args.top_part
    bottom = args.bottom_part

    if args.invert_parts:
      top, bottom = bottom, top

    with open(top) as f:
        TOP_LINES = f.read().split('\n')

    with open(bottom) as f:
        BOTTOM_LINES = f.read().split('\n')

    if not args.number_to_generate:
        NUMBER_TO_GENERATE = len(TOP_LINES)

    print(f'# top parts: {len(TOP_LINES)}, # bottom parts: {len(BOTTOM_LINES)}')


    # Generate a random subset.
    #random.seed(42)
    #indices = np.random.choice(len(TOP_LINES), NUMBER_TO_GENERATE, replace=False)

    indices = range(len(TOP_LINES))
    p = Pool(os.cpu_count())
    #fn = partial(generate_midi, TOP_LINES=None, BOTTOM_LINES=None)
    p.map(generate_midi_single, indices)

    print('DONE!')

