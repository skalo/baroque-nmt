export CUDA_VISIBLE_DEVICES=0
python train.py -data data/C/twopart -save_model twopart-model -world_size 1 -gpu_ranks 0 -valid_steps 20 -save_checkpoint_steps 20
