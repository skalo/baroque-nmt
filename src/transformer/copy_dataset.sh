# To get torchtext to work, install torchtext 0.4.0 from github:
# pip install git+https://github.com/pytorch/text


mkdir -p data/

rm data/*pt
cp ../../../data/baroque_large_trainset_twopart/augmented/training_set/all_input.txt data/src_train.txt
cp ../../../data/baroque_large_trainset_twopart/augmented/training_set/all_output.txt data/tgt_train.txt
cp ../../../data/baroque_large_trainset_twopart/augmented/validation_set/all_input.txt data/src_val.txt
cp ../../../data/baroque_large_trainset_twopart/augmented/validation_set/all_output.txt data/tgt_val.txt

python preprocess.py -train_src data/src_train.txt \
                     -train_tgt data/tgt_train.txt \
                     -valid_src data/src_val.txt \
                     -valid_tgt data/tgt_val.txt \
                     -save_data data/twopart \
                     -shard_size 2000000



