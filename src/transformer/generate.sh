
#python translate.py -model twopart-model_step_840.pt -src data/src_val.txt -output pred.txt -replace_unk -verbose
 
#python translate.py -model transformer-model_step_2500.pt -src data/src_val.txt -output pred-transformer_val.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 20 \
#  -beam_size 20


# Generate for the human subset of testset.

# TRANSFORMER


#python translate.py -model transformer-model_step_49500.pt -src data/src_train.txt -output pred-transformer_test_val.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 40 \
#  -beam_size 20
python translate.py -model transformer-model_step_1500.pt -src data/src_val.txt -output pred-transformer_test_val.txt -replace_unk -verbose \
  -gpu 0 -batch_size 10 \
  -beam_size 20
#python translate.py -model transformer-model_step_8500.pt -src ../../../data/baroque_testset_twopart/validation_set/all_input.txt -output pred-transformer_test_val.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 5 \
#  -beam_size 20
#python translate.py -model transformer-model_step_1500.pt -src ../../../data/baroque_testset_twopart/validation_set/all_input.txt -output pred-transformer_test_val.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 5 \
#  -beam_size 20

#python translate.py -model transformer-nodur-model_step_1500.pt -src ../../../data/baroque_testset_twopart/validation_set/all_input.txt -output pred-transformer_test_val.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 20 \
#  -beam_size 20

# SEQ2SEQ
#python translate.py -model transformer-model_step_1500.pt -src ../../../data/baroque_testset_twopart/validation_set/all_input.txt -output pred-transformer_test_val.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 20 \
#  -beam_size 20





# Generate for the whole test set.
#python translate.py -model transformer-model_step_2500.pt -src ../../../data/baroque_testset_twopart/training_set/all_input.txt -output pred-transformer_test_train.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 20 \
#  -beam_size 20







#python translate.py -model transformer-model_step_1600.pt -src data/src_val.txt -output pred-transformer.txt -replace_unk -verbose \
#  -gpu 0 -batch_size 1000 \
#  -random_sampling_topk 3 -beam_size 1

